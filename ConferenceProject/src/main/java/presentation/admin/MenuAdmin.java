package presentation.admin;

import javax.swing.JFrame;

import presentation.Login;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuAdmin implements ActionListener{

	JFrame frmMenu;
	Login login;
	private BileteView bView;
	private CasieriView cView;
	JButton btnBilet, btnCasier;
	/**
	 * Create the application.
	 */
	public MenuAdmin(Login login) {
		this.login = login;
		initialize();
		frmMenu.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMenu = new JFrame();
		frmMenu.setTitle("Menu");
		frmMenu.setBounds(700, 300, 350, 165);
		frmMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenu.setResizable(false);
		frmMenu.getContentPane().setLayout(null);
		
		btnBilet = new JButton("Bilete");
		btnBilet.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBilet.setBounds(38, 40, 112, 53);
		btnBilet.addActionListener(this);
		frmMenu.getContentPane().add(btnBilet);
		
		btnCasier = new JButton("Casieri");
		btnCasier.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCasier.setBounds(189, 40, 103, 53);
		btnCasier.addActionListener(this);
		frmMenu.getContentPane().add(btnCasier);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnBilet) {
			bView = new BileteView(this);
		} else if (e.getSource() == btnCasier) {
			cView = new CasieriView(this);
		}
		frmMenu.setVisible(false);
		System.gc();
	}
}
