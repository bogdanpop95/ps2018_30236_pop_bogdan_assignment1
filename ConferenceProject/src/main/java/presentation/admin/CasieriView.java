package presentation.admin;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import businessLayer.AdminService;
import businessLayer.CasierService;
import dataAccessLayer.dao.AdminDAO;
import model.CasierBilet;
import model.User;
import presentation.utilities.RefreshTable;
import java.awt.Font;

public class CasieriView implements ActionListener{

	JFrame frmUser;
	Container cPane;
	JLabel jlbName, jlbActive, jlbRole, jlbPassword;
	JTextField jtfName, jtfActive, jtfRole, jtfPassword, jtfFindById;
	JButton jbAdd, jbEdit, jbDelete, jbFindById, jbMenu, btnRaport1, btnRaport2, btnRaport3, btnLogOut;
	JScrollPane js;
	JTable jTable;
	MenuAdmin menu;
	
	AdminService cService;
	CasierService casierService;
	RefreshTable<User> refresher;
	String[] data;
	AdminDAO uDAO;
	
	public CasieriView(MenuAdmin menu) {
		this.menu = menu;
		initialize();
		frmUser.setVisible(true);
		update();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		uDAO = new AdminDAO();
		cService = new AdminService(uDAO);
		casierService = new CasierService();
		
		frmUser = new JFrame("User");
		frmUser.setBounds(500, 200, 900, 600);
		frmUser.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUser.setResizable(false);
		cPane = frmUser.getContentPane();
		cPane.setLayout(null);
		
		refresher = new RefreshTable(uDAO, cPane, js, jTable);
		
		arrangeComponents();
	}
	
	/**
	 * Aranjarea componentelor din frame (ContentPane)
	 */
	private void arrangeComponents() {
		jlbName = new JLabel("name");
		jlbName.setBounds(80, 400, 110, 30);
		jlbName.setFont(jlbName.getFont().deriveFont(16f));
		cPane.add(jlbName);
		
		jlbPassword = new JLabel("password");
		jlbPassword.setBounds(200, 400, 110, 30);
		jlbPassword.setFont(jlbPassword.getFont().deriveFont(16f));
		cPane.add(jlbPassword);
		
		jlbRole = new JLabel("role");
		jlbRole.setBounds(375, 400, 110, 30);
		jlbRole.setFont(jlbRole.getFont().deriveFont(16f));
		cPane.add(jlbRole);
		
		jlbActive = new JLabel("active");
		jlbActive.setBounds(540, 400, 110, 30);
		jlbActive.setFont(jlbActive.getFont().deriveFont(16f));
		cPane.add(jlbActive);
		
		jtfName = new JTextField("");
		jtfName.setBounds(30, 440, 140, 30);
		jtfName.setFont(jtfName.getFont().deriveFont(16f));;
		cPane.add(jtfName);
		
		jtfPassword = new JTextField("");
		jtfPassword.setBounds(185, 440, 140, 30);
		jtfPassword.setFont(jtfPassword.getFont().deriveFont(16f));;
		cPane.add(jtfPassword);
		
		jtfRole = new JTextField("");
		jtfRole.setBounds(340, 440, 140, 30);
		jtfRole.setFont(jtfRole.getFont().deriveFont(16f));;
		cPane.add(jtfRole);
		
		jtfActive = new JTextField("");
		jtfActive.setBounds(495, 440, 140, 30);
		jtfActive.setFont(jtfActive.getFont().deriveFont(16f));;
		cPane.add(jtfActive);
		
		jbAdd = new JButton("Add");
		jbAdd.setBounds(780, 430, 90, 40);
		jbAdd.setFont(jbAdd.getFont().deriveFont(16f));
		jbAdd.addActionListener(this);
		cPane.add(jbAdd);
		
		jbEdit = new JButton("Edit");
		jbEdit.setBounds(780, 200, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);
		
		jbDelete = new JButton("Delete");
		jbDelete.setBounds(780, 310, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(780, 50, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(780, 100, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
		
		jbMenu = new JButton("Meniu");
		jbMenu.setBounds(658, 498, 100, 50);
		jbMenu.setFont(jbMenu.getFont().deriveFont(18f));
		jbMenu.addActionListener(this);
		cPane.add(jbMenu);
		
		btnRaport1 = new JButton("Raport 1");
		btnRaport1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnRaport1.setBounds(69, 498, 121, 50);
		btnRaport1.addActionListener(this);
		cPane.add(btnRaport1);
		
		btnRaport2 = new JButton("Raport 2");
		btnRaport2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnRaport2.setBounds(269, 498, 110, 50);
		btnRaport2.addActionListener(this);
		cPane.add(btnRaport2);
		
		btnRaport3 = new JButton("Raport 3");
		btnRaport3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnRaport3.setBounds(464, 498, 110, 50);
		btnRaport3.addActionListener(this);
		cPane.add(btnRaport3);
		
		btnLogOut = new JButton("Log Out");
		btnLogOut.setBounds(795, 515, 89, 23);
		btnLogOut.addActionListener(this);
		frmUser.getContentPane().add(btnLogOut);
	}
	
	public void update() {
		cPane = refresher.refresh(null);
		jTable = refresher.getJTable();
		js = refresher.getJScrollPane();
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbAdd) {
			try {
				User user = new User(jtfName.getText(), jtfPassword.getText(), jtfRole.getText(), Integer.parseInt(jtfActive.getText()));
				cService.insertUser(user);
			} catch (Exception exception) {
				JOptionPane.showMessageDialog(frmUser, exception.getMessage(), "Could not insert into database!", JOptionPane.ERROR_MESSAGE);
			}
			jtfName.setText(null); jtfPassword.setText(null); jtfRole.setText(null); jtfActive.setText(null);
			update();
			System.gc();
		} else if (e.getSource() == jbEdit) {
			int row = jTable.getSelectedRow();
			int columns = jTable.getColumnCount();
			System.out.println((Integer)jTable.getValueAt(row, 0));
			//User user = cService.findUserById((Integer)jTable.getValueAt(row, 0));
			data = new String[columns];
			for (int i = 0; i < columns; i++) {
				data[i] = jTable.getValueAt(row, i).toString();
			}
			User newUser = new User(data[0], data[1], data[2], data[3], data[4]);
			try {
				for (int i = 1; i < columns; i++) {
						cService.editUser(newUser, jTable.getColumnName(i), data[i].toString());
					}
				JOptionPane.showMessageDialog(frmUser, "Edit successful!", "Edit info", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception exception) {	
				JOptionPane.showMessageDialog(frmUser, exception.getMessage(), "Could not insert into database!", JOptionPane.ERROR_MESSAGE);
			}
			update();
			System.gc();
		} else if (e.getSource() == jbDelete) {
			int row = jTable.getSelectedRow();
			System.out.println((Integer)jTable.getValueAt(row, 0));
			User user = cService.findUserById((Integer)jTable.getValueAt(row, 0));
			try {
				cService.deleteUser(user);
			} catch (Exception exception) {
				JOptionPane.showMessageDialog(frmUser, exception.getMessage(), "!", JOptionPane.ERROR_MESSAGE);
			}
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			try {
				User user = cService.findUserById(Integer.parseInt(jtfFindById.getText()));
				for (int i = 0; i < jTable.getRowCount(); i++) {
					if ((Integer)jTable.getValueAt(i, 0) == user.getIdUser()) {
						jTable.setRowSelectionInterval(i, i);
					}
				}
			} catch (Exception exception) {
				JOptionPane.showMessageDialog(frmUser, exception.getMessage(), "Invalid id!", JOptionPane.ERROR_MESSAGE);
			}
			jtfFindById.setText("");
		} else if (e.getSource() == jbMenu) {
			frmUser.setVisible(false);
			menu.frmMenu.setVisible(true);
		} else if (e.getSource() == btnRaport1) {
			int row = jTable.getSelectedRow();
			int nrBilete = 0;
			try {
				String name = jTable.getValueAt(row, 1).toString();
				String idUser = jTable.getValueAt(row, 0).toString();
				nrBilete = cService.raport1(Integer.parseInt(idUser));
				JOptionPane.showMessageDialog(frmUser, "Casierul " + name + "(id = " + idUser + ") a vandut: " + nrBilete + " bilete", "Raport1 info", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception exception) {
					JOptionPane.showMessageDialog(frmUser, "Please select a cashier!", "Info", JOptionPane.INFORMATION_MESSAGE);
			}
		} else if (e.getSource() == btnRaport2) {
			List<CasierBilet> list = casierService.getcDAO().findAll();
			String[] choices = new String[list.size()];
			int i = 0;
			for(CasierBilet cb : list) {
				choices[i++] = cb.getDate().toString();
			}
			String input = (String) JOptionPane.showInputDialog(frmUser, "Choose day", "Day to choose", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date date = formatter.parse(input);
				System.out.println(date.toString());
				int nrBilete = cService.raport2(date);
				JOptionPane.showMessageDialog(frmUser, "In ziua " + input.toString() + " s-au vandut: " + nrBilete + " bilete", "Raport2 info", JOptionPane.INFORMATION_MESSAGE);
			} catch (ParseException exc) {
				exc.printStackTrace();
			}
		} else if (e.getSource() == btnRaport3) {
			int suma = cService.raport3();
			JOptionPane.showMessageDialog(frmUser, "S-au vandut un total de: " + suma, "Raport3 info", JOptionPane.INFORMATION_MESSAGE);
		} else if (e.getSource() == btnLogOut) {
			frmUser.setVisible(false);
			menu.login.getFrame().setVisible(true);
		}
	}
}
