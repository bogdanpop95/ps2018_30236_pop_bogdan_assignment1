package presentation.admin;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import businessLayer.BiletService;
import businessLayer.CasierService;
import dataAccessLayer.dao.BiletDAO;
import model.Bilet;
import presentation.Login;
import presentation.utilities.RefreshTable;
import java.awt.Font;

public class BileteView implements ActionListener{

	JFrame frmBilete;
	Container cPane;
	JLabel jlbPrice, jlbDisponibile, jlbDay, jlbDescription;
	JTextField jtfPrice, jtfDisponibile, jtfDay, jtfDescription, jtfFindById;
	JButton jbAdd, jbEdit, jbDelete, jbFindById, jbMenu, btnMax, btnLogOut;
	JScrollPane js;
	JTable jTable;
	MenuAdmin menu;
	
	BiletService bService;
	RefreshTable<Bilet> refresher;
	String[] data;
	BiletDAO bDAO;
	
	public BileteView(MenuAdmin menu) {
		this.menu = menu;
		initialize();
		frmBilete.setVisible(true);
		update();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		bDAO = new BiletDAO();
		bService = new BiletService(bDAO);
		
		frmBilete = new JFrame("Bilete");
		frmBilete.setBounds(500, 200, 900, 600);
		frmBilete.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBilete.setResizable(false);
		cPane = frmBilete.getContentPane();
		cPane.setLayout(null);
		
		refresher = new RefreshTable(bDAO, cPane, js, jTable);
		
		arrangeComponents();
	}
	
	/**
	 * Aranjarea componentelor din frame (ContentPane)
	 */
	private void arrangeComponents() {
		jlbPrice = new JLabel("price");
		jlbPrice.setBounds(80, 400, 110, 30);
		jlbPrice.setFont(jlbPrice.getFont().deriveFont(16f));
		cPane.add(jlbPrice);
		
		jlbDescription = new JLabel("description");
		jlbDescription.setBounds(200, 400, 110, 30);
		jlbDescription.setFont(jlbDescription.getFont().deriveFont(16f));
		cPane.add(jlbDescription);
		
		jlbDay = new JLabel("day");
		jlbDay.setBounds(375, 400, 110, 30);
		jlbDay.setFont(jlbDay.getFont().deriveFont(16f));
		cPane.add(jlbDay);
		
		jlbDisponibile = new JLabel("Disponibile");
		jlbDisponibile.setBounds(540, 400, 110, 30);
		jlbDisponibile.setFont(jlbDisponibile.getFont().deriveFont(16f));
		cPane.add(jlbDisponibile);
		
		jtfPrice = new JTextField("");
		jtfPrice.setBounds(30, 440, 140, 30);
		jtfPrice.setFont(jtfPrice.getFont().deriveFont(16f));;
		cPane.add(jtfPrice);
		
		jtfDescription = new JTextField("");
		jtfDescription.setBounds(185, 440, 140, 30);
		jtfDescription.setFont(jtfDescription.getFont().deriveFont(16f));;
		cPane.add(jtfDescription);
		
		jtfDay = new JTextField("");
		jtfDay.setBounds(340, 440, 140, 30);
		jtfDay.setFont(jtfDay.getFont().deriveFont(16f));;
		cPane.add(jtfDay);
		
		jtfDisponibile = new JTextField("");
		jtfDisponibile.setBounds(495, 440, 140, 30);
		jtfDisponibile.setFont(jtfDisponibile.getFont().deriveFont(16f));;
		cPane.add(jtfDisponibile);
		
		jbAdd = new JButton("Add");
		jbAdd.setBounds(780, 430, 90, 40);
		jbAdd.setFont(jbAdd.getFont().deriveFont(16f));
		jbAdd.addActionListener(this);
		cPane.add(jbAdd);
		
		jbEdit = new JButton("Edit");
		jbEdit.setBounds(780, 200, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);
		
		jbDelete = new JButton("Delete");
		jbDelete.setBounds(780, 310, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(780, 50, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(780, 100, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
		
		jbMenu = new JButton("Meniu");
		jbMenu.setBounds(400, 500, 100, 50);
		jbMenu.setFont(jbMenu.getFont().deriveFont(18f));
		jbMenu.addActionListener(this);
		cPane.add(jbMenu);
		
		btnMax = new JButton("MAX_CAPACITY");
		btnMax.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnMax.setBounds(639, 500, 131, 50);
		btnMax.addActionListener(this);
		cPane.add(btnMax);
		
		btnLogOut = new JButton("Log Out");
		btnLogOut.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnLogOut.setBounds(48, 500, 122, 50);
		btnLogOut.addActionListener(this);
		cPane.add(btnLogOut);
	}
	
	public void update() {
		cPane = refresher.refresh(null);
		jTable = refresher.getJTable();
		js = refresher.getJScrollPane();
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbAdd) {
			try {
				Bilet bilet = new Bilet(Integer.parseInt(jtfPrice.getText()), jtfDescription.getText(), Integer.parseInt(jtfDay.getText()), Integer.parseInt(jtfDisponibile.getText()));
				bService.insertBilet(bilet);
			} catch (Exception exception) {
				JOptionPane.showMessageDialog(frmBilete, exception.getMessage(), "Could not insert into database!", JOptionPane.ERROR_MESSAGE);
			}
			jtfPrice.setText(null); jtfDescription.setText(null); jtfDay.setText(null); jtfDisponibile.setText(null);
			update();
			System.gc();
		} else if (e.getSource() == jbEdit) {
			int row = jTable.getSelectedRow();
			int columns = jTable.getColumnCount();
			System.out.println((Integer)jTable.getValueAt(row, 0));
			//Bilet bilet = bService.findBiletById((Integer)jTable.getValueAt(row, 0));
			data = new String[columns];
			for (int i = 0; i < columns; i++) {
				data[i] = jTable.getValueAt(row, i).toString();
			}
			Bilet newBilet = new Bilet(data[0], data[1], data[2], data[3], data[4]);
			try {
				for (int i = 1; i < columns; i++) {
						bService.editBilet(newBilet, jTable.getColumnName(i), data[i].toString());
					}
				JOptionPane.showMessageDialog(frmBilete, "Edit successful!", "Edit info", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception exception) {	
				JOptionPane.showMessageDialog(frmBilete, exception.getMessage(), "Could not insert into database!", JOptionPane.ERROR_MESSAGE);
			}
			update();
			System.gc();
		} else if (e.getSource() == jbDelete) {
			int row = jTable.getSelectedRow();
			System.out.println((Integer)jTable.getValueAt(row, 0));
			Bilet bilet = bService.findBiletById((Integer)jTable.getValueAt(row, 0));
			try {
				bService.deleteBilet(bilet);
			} catch (Exception exception) {
				JOptionPane.showMessageDialog(frmBilete, exception.getMessage(), "!", JOptionPane.ERROR_MESSAGE);
			}
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			try {
				Bilet bilet = bService.findBiletById(Integer.parseInt(jtfFindById.getText()));
				for (int i = 0; i < jTable.getRowCount(); i++) {
					if ((Integer)jTable.getValueAt(i, 0) == bilet.getIdBilet()) {
						jTable.setRowSelectionInterval(i, i);
					}
				}
			} catch (Exception exception) {
				JOptionPane.showMessageDialog(frmBilete, exception.getMessage(), "Invalid id!", JOptionPane.ERROR_MESSAGE);
			}
			jtfFindById.setText("");
		} else if (e.getSource() == jbMenu) {
			frmBilete.setVisible(false);
			menu.frmMenu.setVisible(true);
		} else if (e.getSource() == btnMax) {
			int cap = 0, maxim = 0;
			for (int i = 0; i < 3; i++) {
				if (CasierService.sold[i] > maxim)
					maxim = CasierService.sold[i];
			}
			String input = (String) JOptionPane.showInputDialog("Actual value is: " + CasierService.MAX_CAPACITY, "Change value");
			if (input != null)
				try {
					cap = Integer.parseInt(input);
					if (cap <= 0) {
						JOptionPane.showMessageDialog(frmBilete, "Invalid value, too small!", "Invalid!", JOptionPane.ERROR_MESSAGE);
					} else if ((maxim + CasierService.sold[3]) > cap) {
						JOptionPane.showMessageDialog(frmBilete, "Invalid value, No. of tickets sold is bigger!", "Invalid!", JOptionPane.ERROR_MESSAGE);
					}
					CasierService.MAX_CAPACITY = cap;
					JOptionPane.showMessageDialog(frmBilete, "Value Changed!", "MAX_CAPACITY info", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception exception) {
					JOptionPane.showMessageDialog(frmBilete, exception.getMessage(), "Invalid value!", JOptionPane.ERROR_MESSAGE);
				}
		} else if (e.getSource() == btnLogOut) { 
			frmBilete.setVisible(false);
			menu.login.getFrame().setVisible(true);
		}
	}
}
