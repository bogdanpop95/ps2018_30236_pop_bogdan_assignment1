package presentation.casier;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.CasierService;
import model.Bilet;
import model.CasierBilet;
import model.User;
import presentation.Login;
import presentation.utilities.RefreshTable;

public class CasierView implements ActionListener{

	JFrame frmCasier;
	JScrollPane js;
	JTable jTable;
	Container cPane;
	JButton btnDay1, btnDay2, btnDay3, btnDay4, btnLogOut;
	
	Object[][] line;

	private User casier;
	private CasierService cService;
	private RefreshTable<CasierBilet> refresher;
	
	Login login;

	public CasierView(User casier, Login login) {
		this.casier = casier;
		this.login = login;
		initialize();
		frmCasier.setVisible(true);
		update();
	}

	/**
	 * Initialize the contents of the frmCasier.
	 */
	private void initialize() {
		cService = new CasierService();
		
		frmCasier = new JFrame("Casier " + casier.getName());
		frmCasier.setBounds(500, 200, 900, 600);
		frmCasier.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCasier.setResizable(false);
		cPane = frmCasier.getContentPane();
		cPane.setLayout(null);
		
		refresher = new RefreshTable(cService.getcDAO(), cPane, js, jTable);
		
		arrangeComponents();
	}
	
	private void arrangeComponents() {
		btnDay1 = new JButton("1-Day Pass");
		btnDay1.setBounds(60, 485, 125, 58);
		btnDay1.addActionListener(this);
		cPane.add(btnDay1);
		
		btnDay2 = new JButton("2-Day Pass");
		btnDay2.setBounds(250, 485, 125, 58);
		btnDay2.addActionListener(this);
		cPane.add(btnDay2);
		
		btnDay3 = new JButton("3-Day Pass");
		btnDay3.setBounds(450, 485, 125, 58);
		btnDay3.addActionListener(this);
		cPane.add(btnDay3);
		
		btnDay4 = new JButton("All-day Pass");
		btnDay4.setBounds(650, 485, 125, 58);
		btnDay4.addActionListener(this);
		cPane.add(btnDay4);
		
		btnLogOut = new JButton("Log Out");
		btnLogOut.setBounds(796, 485, 98, 58);
		btnLogOut.addActionListener(this);
		cPane.add(btnLogOut);
	}
	
	private void update() {
		cPane = refresher.refresh(casier);
		jTable = refresher.getJTable();
		js = refresher.getJScrollPane();
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnDay1) {
			sellDay(1);
		} else if (e.getSource() == btnDay2) {
			sellDay(2);
		} else if (e.getSource() == btnDay3) {
			sellDay(3);
		} else if (e.getSource() == btnDay4) {
			sellDay(4);
		} else if (e.getSource() == btnLogOut) {
			frmCasier.setVisible(false);
			login.getFrame().setVisible(true);
		}
		update();
		System.gc();
	}
	
	public void sellDay(int day) {
		try {
			cService.sell(day);
			JOptionPane.showMessageDialog(frmCasier, "Printare bilet in progres!", "Print", JOptionPane.INFORMATION_MESSAGE);
			for (Bilet b : cService.getBilete()) {
				if (b.getDay() == day) {
					cService.print(casier, b);
					cService.insert(b, casier);
				}
			}
		} catch (Exception exception) {
			JOptionPane.showMessageDialog(frmCasier, exception.getMessage(), "!", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
