package presentation;



import businessLayer.LoginService;
import dataAccessLayer.dao.LoginDAO;
import model.User;
import presentation.admin.MenuAdmin;
import presentation.casier.CasierView;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login implements ActionListener{

	JFrame frame;
	JFrame exitFrame;
	private JTextField jtfName;
	private JPasswordField jpsPass;
	private JLabel jlbLogin, jlbName, jlbPass;
	private JButton btnLogin, btnReset, btnExit;
	private JSeparator separator1, separator2;
	
	LoginService loginService;
	User user;
	MenuAdmin adminView;
	CasierView casierView;
	
	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		loginService = new LoginService(new LoginDAO());
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Login");
		frame.setBounds(700, 300, 500, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		jlbLogin = new JLabel("Login");
		jlbLogin.setBounds(240, 11, 56, 14);
		frame.getContentPane().add(jlbLogin);
		
		jlbName = new JLabel("Username");
		jlbName.setBounds(115, 98, 62, 14);
		frame.getContentPane().add(jlbName);
		
		jlbPass = new JLabel("Password");
		jlbPass.setBounds(115, 158, 62, 14);
		frame.getContentPane().add(jlbPass);
		
		jtfName = new JTextField();
		jtfName.setBounds(187, 95, 153, 20);
		frame.getContentPane().add(jtfName);
		jtfName.setColumns(10);
		
		jpsPass = new JPasswordField();
		jpsPass.setBounds(187, 155, 153, 20);
		frame.getContentPane().add(jpsPass);
		
		btnLogin = new JButton("Login");
		btnLogin.setBounds(207, 208, 89, 23);
		btnLogin.addActionListener(this);
		frame.getContentPane().add(btnLogin);
		
		btnReset = new JButton("Reset");
		btnReset.setBounds(46, 208, 89, 23);
		btnReset.addActionListener(this);
		frame.getContentPane().add(btnReset);
		
		btnExit = new JButton("Exit");
		btnExit.setBounds(366, 208, 89, 23);
		btnExit.addActionListener(this);
		frame.getContentPane().add(btnExit);
		
		separator1 = new JSeparator();
		separator1.setBounds(10, 193, 464, 2);
		frame.getContentPane().add(separator1);
		
		separator2 = new JSeparator();
		separator2.setBounds(10, 82, 464, 2);
		frame.getContentPane().add(separator2);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnReset) {
			jtfName.setText("");
			jpsPass.setText("");
		} else if (e.getSource() == btnExit) {
			exitFrame = new JFrame("Exit");
			if (JOptionPane.showConfirmDialog(exitFrame, "Confirm if you want to exit", "Login",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
				System.exit(0);
			}
		} else if (e.getSource() == btnLogin) {
			try {
				user = loginService.findUser(jtfName.getText(), jpsPass.getText());
				if (user.getActive() == 0) {
					JOptionPane.showMessageDialog(frame, "Ne pare rau, contul d-voastra este dezactivat!\nContactati Administratorul.", "Sorry!", JOptionPane.INFORMATION_MESSAGE);
				} else {
					if (user.getRole().equals("admin")) {
						adminView = new MenuAdmin(this);
					} else if (user.getRole().equals("casier")) {
						casierView = new CasierView(user, this);
					}
				}
				frame.setVisible(false);
			} catch (Exception exception){
				JOptionPane.showMessageDialog(frame, exception.getMessage(), "Try again!", JOptionPane.ERROR_MESSAGE);
			} finally {
				jtfName.setText(null);
				jpsPass.setText(null);
			}
		}
		System.gc();
	}
	
	public JFrame getFrame() {
		return frame;
	}
}
