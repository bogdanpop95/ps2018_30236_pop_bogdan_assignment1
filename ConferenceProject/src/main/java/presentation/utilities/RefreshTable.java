package presentation.utilities;

import java.awt.Container;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import dataAccessLayer.dao.AbstractDAO;
import dataAccessLayer.reflection.Reflection;
import model.User;



/**
 * @author pop_b
 * Clasa are scop implementarea unui refresher pentru tabelele care afiseaza continutul bazei de date
 * dupa fiecare operatie asupra ei
 * Se folosesc parametrii generici si reflexivitatea
 * @param <T>
 */
public class RefreshTable<T> {
	
	private Container cPane;
	private JScrollPane js;
	private JTable jTable;
	private List<T> elements;
	AbstractDAO<T> dao;
	List<String> columns;
	Object[][] line;
	List<Object> values;
	
	public RefreshTable(AbstractDAO<T> dao,Container cPane, JScrollPane js, JTable jTable) {
		this.dao = dao;
		this.cPane = cPane;
		this.js = js;
		this.jTable = jTable;
	}
	
	public Container refresh(User user) {
		if (user == null)
			elements = dao.findAll();
		else
			elements = dao.findAll(user);
		if (elements.isEmpty())
			return cPane;
		columns = dao.getFields1(elements.get(0));
		line = new Object[elements.size()][columns.size()];
		if (js != null)
			cPane.remove(js);
		if (jTable != null)
			cPane.remove(jTable);
		cPane.revalidate();
		js = new JScrollPane();
		js.setBounds(50, 50, 700, 300);
		int i = 0, j;
		for (T t : elements) {
			j = 0;
			values = Reflection.retrieveProprieties(t);
			for (Object obj : values) {
				line[i][j++] = obj;
			}
			i++;
		}
		Object[] col = columns.toArray();
		jTable = new JTable(line, col);
		js.setViewportView(jTable);
		js.repaint();
		jTable.repaint();
		cPane.add(js);
		return cPane;
	}
	
	public JTable getJTable() {
		return jTable;
	}
	
	public JScrollPane getJScrollPane() {
		return js;
	}
	
}
