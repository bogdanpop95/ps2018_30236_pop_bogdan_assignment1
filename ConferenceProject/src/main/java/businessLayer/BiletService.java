package businessLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLayer.validators.Validator;
import businessLayer.validators.biletValidators.DayValidator;
import businessLayer.validators.biletValidators.DisponibileValidator;
import businessLayer.validators.biletValidators.PriceValidator;
import dataAccessLayer.dao.BiletDAO;
import model.Bilet;

public class BiletService {

	private BiletDAO bDAO;
	private List<Validator<Bilet>> validators;
	
	public BiletService(BiletDAO bDAO) {
		this.bDAO = bDAO;
		validators = new ArrayList<Validator<Bilet>>();
		validators.add(new DayValidator());
		validators.add(new PriceValidator());
		validators.add(new DisponibileValidator());
	}
	
	public int insertBilet(Bilet bilet) throws Exception {
		try {
			for (Validator<Bilet> v : validators) {
				v.validate(bilet);
			}
			return bDAO.insert(bilet);
		} catch (Exception e) {
			throw e;
		}

	}
	
	public void editBilet(Bilet bilet, String columnName, String newValue) throws Exception {
		try {
			for (Validator<Bilet> v : validators) {
					v.validate(bilet);
				} 
			bDAO.edit(bilet, columnName, newValue);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteBilet(Bilet bilet) throws Exception {
		bDAO.delete(bilet);
	}
	
	public Bilet findBiletById(int idBilet) {
		Bilet bilet = bDAO.findById(idBilet);
		
		if (bilet == null) {
			throw new NoSuchElementException("The ticket with id = " + idBilet + "not found!");
		}
		
		return bilet;
	}
	
	public List<Bilet> findAllBilet() {
		List<Bilet> lb = bDAO.findAll();
		
		return lb;
	}
	
	public BiletDAO getBiletDAO() {
		return bDAO;
	}
}
