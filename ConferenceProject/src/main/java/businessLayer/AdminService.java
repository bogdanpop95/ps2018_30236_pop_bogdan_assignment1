package businessLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import businessLayer.validators.Validator;
import businessLayer.validators.userValidators.*;
import dataAccessLayer.dao.AdminDAO;
import model.User;

public class AdminService {
	
	private AdminDAO uDAO;
	private List<Validator<User>> validators;
	
	public AdminService(AdminDAO uDAO) {
		this.uDAO = uDAO;
		validators = new ArrayList<Validator<User>>();
		validators.add(new NameValidator());
		validators.add(new ActiveValidator());
		validators.add(new RoleValidator());
	}
	
	public int insertUser(User user) throws Exception {
		try {
			for (Validator<User> v : validators) {
				v.validate(user);
			}
			return uDAO.insert(user);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void editUser(User user, String columnName, String newValue) throws Exception {
		try {
			for (Validator<User> v : validators) {
					v.validate(user);
				} 
			uDAO.edit(user, columnName, newValue);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteUser(User user) throws Exception {
		uDAO.delete(user);
	}
	
	public User findUserById(int idUser) {
		User user = uDAO.findById(idUser);
		
		if (user == null) {
			throw new NoSuchElementException("The user with id = " + idUser + "not found!");
		}
		
		return user;
	}
	
	public List<User> findAllUser() {
		List<User> lb = uDAO.findAll();
		
		return lb;
	}
	
	public int raport1(int idUser) {
		return uDAO.raport1(idUser);
	}
	
	public int raport2(Date date) {
		return uDAO.raport2(date);
	}
	
	public int raport3() {
		return uDAO.raport3();
	}
}
