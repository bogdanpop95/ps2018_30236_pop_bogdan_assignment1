package businessLayer;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JOptionPane;

import dataAccessLayer.dao.BiletDAO;
import dataAccessLayer.dao.CasierDAO;
import model.Bilet;
import model.CasierBilet;
import model.User;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class CasierService {
	
	public static int MAX_CAPACITY = 105;
	public static int[] sold = {0, 0, 0, 0};
	private BiletService bService;
	private CasierDAO cDAO;
	
	public CasierService() {
		this.bService = new BiletService(new BiletDAO());
		this.cDAO = new CasierDAO();
	}

	public BiletService getbService() {
		return bService;
	}
	
	public CasierDAO getcDAO() {
		return cDAO;
	}
	
	public List<CasierBilet> findAll(User casier) {
		List<CasierBilet> ls = cDAO.findAll(casier);
		
		if (ls.isEmpty()) {
			throw new NoSuchElementException("No tickets sold yet!");
		}
		
		return ls;
	}
	
	public void sell(int day) throws Exception {
		Bilet bilet = null;
		List<Bilet> bilete = getBilete();
		
		for (Bilet b : bilete) {
			if (b.getDay() == day)
				bilet = b;
		}
		Integer disponibile = bilet.getDisponibile() - 1;
		sold[bilet.getDay() - 1]++;
		if (bilet.getDay() != 4) {
			if (sold[bilet.getDay() - 1] + sold[3] > MAX_CAPACITY)
				throw new NoSuchElementException("Max_capacity(" + MAX_CAPACITY + ") reached!");
		}
		if (bilet.getDay() == 4) {
			if (Math.max((Math.max(sold[0], sold[1])), sold[2]) + sold[3] > MAX_CAPACITY)
				throw new NoSuchElementException("Max_capacity(" + MAX_CAPACITY + ") reached!");
		}
		if (disponibile < 0)
			throw new NoSuchElementException("Tickets are sold out!");
		else
			bService.editBilet(bilet, "disponibile", disponibile.toString());
		
	}
	
	public List<Bilet> getBilete() {
		List<Bilet> lb = bService.findAllBilet();
		
		return lb;
	}
	
	public void print(User casier, Bilet bilet) {
		Document document = new Document(PageSize.A4);
		document.addAuthor(casier.getName() + casier.getIdUser());
		document.addTitle("BiletDay" + bilet.getDay() + "-" + bilet.getDisponibile() + 1);
		try {
			if (bilet.getDay() < 4)
				PdfWriter.getInstance(document, new FileOutputStream("tickets\\day" + bilet.getDay() + "\\BiletDay" + bilet.getDay() + "-" + (bilet.getDisponibile() + 1) + ".pdf"));
			else
				PdfWriter.getInstance(document, new FileOutputStream("tickets\\allDays" + "\\BiletDay" + bilet.getDay() + "-" + (bilet.getDisponibile() + 1) + ".pdf"));
			document.open();
			if (bilet.getDay() < 4)
				document.add(new Paragraph(bilet.getDay() + "-Day Pass"));
			else
				document.add(new Paragraph("All-Day Pass"));
			// Description
			document.add(new Paragraph("Description:"));
			document.add(new Paragraph(bilet.getDescription()));
			JOptionPane.showMessageDialog(null, "Order successfully printed!");
		} catch (Exception b) {
			b.printStackTrace();
		} finally {
			document.close();
		}
	}
	
	public void insert(Bilet bilet, User casier) {
		cDAO.insert(bilet, casier, new Date());
	}
}
