package businessLayer;

import java.util.NoSuchElementException;

import dataAccessLayer.dao.LoginDAO;
import model.User;

public class LoginService {
	
	private LoginDAO lDAO;
	
	public LoginService(LoginDAO lDAO) {
		this.lDAO = lDAO;
	}
	
	public User findUser(String name, String password) {
		User user = lDAO.findUser(name, password);
		
		if (user == null) {
			throw new NoSuchElementException("Invalid username/password!");
		}
		return user;
	}
	
}
