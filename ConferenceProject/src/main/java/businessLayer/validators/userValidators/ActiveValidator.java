package businessLayer.validators.userValidators;

import businessLayer.validators.Validator;
import model.User;

public class ActiveValidator implements Validator<User>{
	
	public void validate(User user) {
		if (user.getActive() != 0 && user.getActive() != 1) {
			throw new IllegalArgumentException("Invalid active value! (1 - active or 0 - inactive)");
		}
	}
}
