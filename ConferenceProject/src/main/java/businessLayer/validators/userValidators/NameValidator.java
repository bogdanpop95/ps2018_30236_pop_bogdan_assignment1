package businessLayer.validators.userValidators;

import java.util.regex.Pattern;
import model.User;
import businessLayer.validators.Validator;

public class NameValidator implements Validator<User>{
	
	private static final String NAME_PATTERN = "^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$";
	
	public void validate(User user) {
		Pattern pattern = Pattern.compile(NAME_PATTERN);
		if (!pattern.matcher(user.getName()).matches()) {
			throw new IllegalArgumentException("Invalid name : Valid Characters include (A-Z) (a-z) (0-9) (+_-)");
		}
	}
}