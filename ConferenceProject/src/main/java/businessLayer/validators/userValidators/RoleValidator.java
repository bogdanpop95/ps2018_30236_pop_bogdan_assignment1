package businessLayer.validators.userValidators;

import businessLayer.validators.Validator;
import model.User;

public class RoleValidator implements Validator<User>{
	
	public void validate(User user) {
		if (user.getRole().equals("admin") || user.getRole().equals("casier"))
			;
		else
			throw new IllegalArgumentException("Invalid Role! Only 'admin' or 'casier'.");
	}
	 
}
