package businessLayer.validators.biletValidators;

import businessLayer.validators.Validator;
import model.Bilet;

public class PriceValidator implements Validator<Bilet>{

	public void validate(Bilet bilet) {
		if (bilet.getPrice() < 0) {
			throw new IllegalArgumentException("Invalid price!");
		}
	}
}
