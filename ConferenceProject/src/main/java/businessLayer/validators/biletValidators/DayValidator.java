package businessLayer.validators.biletValidators;

import businessLayer.validators.Validator;
import model.Bilet;

public class DayValidator implements Validator<Bilet>{
	
	public void validate(Bilet bilet) {
		if (bilet.getDay() < 0) {
			throw new IllegalArgumentException("Invalid day!");
		}
	}
}
