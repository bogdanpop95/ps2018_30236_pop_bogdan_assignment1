package businessLayer.validators.biletValidators;

import java.util.List;

import businessLayer.CasierService;
import businessLayer.validators.Validator;
import dataAccessLayer.dao.BiletDAO;
import model.Bilet;

public class DisponibileValidator implements Validator<Bilet>{
	
	BiletDAO biletDAO = new BiletDAO();
	
	public void validate(Bilet bilet) {
		if (bilet.getDay() != 4 && bilet.getDisponibile() > CasierService.MAX_CAPACITY) {
			throw new IllegalArgumentException("Invalid value, over capacity!");
		} else if (bilet.getDay() == 4) {
			List<Bilet> bilete = biletDAO.findAll();
			int max = 0;
			for (Bilet b: bilete) {
				if(b.getDay() != 4)
					if (b.getDisponibile() > max)
						max = b.getDisponibile();
			}
			if (max + bilet.getDisponibile() > CasierService.MAX_CAPACITY)
				throw new IllegalArgumentException("Invalid value, over capacity!");
		}
	}
}
