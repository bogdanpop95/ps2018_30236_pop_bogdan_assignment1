package dataAccessLayer.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import dataAccessLayer.connection.ConnectionFactory;
import model.Bilet;
import model.CasierBilet;
import model.User;

public class CasierDAO extends AbstractDAO<CasierBilet>{
	
	protected static final Logger LOGGER = Logger.getLogger(LoginDAO.class.getName());
	
	private final static String historyStatementString = "SELECT casierbilet.idBilet, bilet.price, bilet.day, casierbilet.soldAt, casierbilet.date"
			+ " FROM casierbilet "
			+ "INNER JOIN bilet ON casierbilet.idBilet = bilet.idBilet "
			+ "WHERE casierbilet.soldBy = ? "
			+ "ORDER BY date DESC, soldat DESC";
	
	private final static String findStatementString = "SELECT casierbilet.idBilet, bilet.price, bilet.day, casierbilet.soldAt, casierbilet.date"
			+ " FROM casierbilet "
			+ "INNER JOIN bilet ON casierbilet.idBilet = bilet.idBilet "
			+ "GROUP BY date "
			+ "ORDER BY date DESC, soldat DESC";
	
	private final static String insertStatementString = "INSERT INTO casierbilet(idBilet, soldBy, soldAt, date) VALUES (?, ?, ?, ?)";
	
	public List<CasierBilet> findAll(User user) {
		List<CasierBilet> toReturn = null;
		
		Connection dbConnection = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			dbConnection = ConnectionFactory.getConnection();
			findStatement = dbConnection.prepareStatement(historyStatementString);
			findStatement.setInt(1, user.getIdUser());
			rs = findStatement.executeQuery();
			toReturn = createObjects(rs);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"LoginDAO:findUser " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
			if (!toReturn.isEmpty())
				System.out.println("SELECT operation on casierBilet completed successfully!");
		}
		return toReturn;
	}
	
	@Override
	public List<CasierBilet> findAll() {
		List<CasierBilet> toReturn = null;
		
		Connection dbConnection = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			dbConnection = ConnectionFactory.getConnection();
			findStatement = dbConnection.prepareStatement(findStatementString);
			rs = findStatement.executeQuery();
			toReturn = createObjects(rs);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"LoginDAO:findUser " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
			if (!toReturn.isEmpty())
				System.out.println("SELECT operation on casierBilet completed successfully!");
		}
		return toReturn;
	}
	
	
	
	public void insert(Bilet bilet, User casier, Date date) {
		
		Connection dbConnection = null;
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		
		try {
			dbConnection = ConnectionFactory.getConnection();
			insertStatement = dbConnection.prepareStatement(insertStatementString);
			insertStatement.setInt(1, bilet.getIdBilet());
			insertStatement.setInt(2, casier.getIdUser());
			insertStatement.setTime(3, new java.sql.Time(date.getTime()));
			insertStatement.setDate(4, new java.sql.Date(date.getTime()));
			insertStatement.executeUpdate();
			
			System.out.println("SELECT operation on casierBilet completed successfully!");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CasierDAO:findUser " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
}
