package dataAccessLayer.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import dataAccessLayer.connection.ConnectionFactory;
import model.User;

/**
* @author pop_b
* Clasa care inglobeaza operatiile CRUD pe Useri si care mosteneste clasa AbstracDAO
*/
public class AdminDAO extends AbstractDAO<User>{
	
	private final static String raport1StatementString = "SELECT COUNT(*) FROM casierbilet "
			+ "WHERE soldBy = ?";
	
	private final static String raport2StatementString = "SELECT COUNT(*) FROM casierbilet "
			+ "WHERE date = ?";
	
	private final static String raport3StatementString = "SELECT SUM(bilet.price)"
			+ " FROM casierbilet "
			+ "INNER JOIN bilet ON casierbilet.idBilet = bilet.idBilet ";
	
	public AdminDAO() {
		super();
	}
	
	public List<User> findAll() {
		return super.findAll();
	}
	
	public User findById(int idUser) {
		return super.findById(idUser);
	}
	
	public int insert(User user) {
		return super.insert(user);
	}
	
	public void edit(User user, String columnName, Object newValue) {
		super.edit(user, columnName, newValue);
	}
	
	public void delete(User user) throws Exception {
		super.delete(user);
	}
	
	public int raport1(int idUser) {
		int toReturn = 0;
		
		Connection dbConnection = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			dbConnection = ConnectionFactory.getConnection();
			findStatement = dbConnection.prepareStatement(raport1StatementString);
			findStatement.setInt(1, idUser);
			rs = findStatement.executeQuery();
			if (rs.next()) {
				toReturn = rs.getInt(1);
			}
			System.out.println("SELECT operation on casierBilet completed successfully!");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"AdminDAO:raport1 " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public int raport2(Date date) {
		int toReturn = 0;
		
		Connection dbConnection = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			dbConnection = ConnectionFactory.getConnection();
			findStatement = dbConnection.prepareStatement(raport2StatementString);
			findStatement.setDate(1, new java.sql.Date(date.getTime()));
			rs = findStatement.executeQuery();
			if (rs.next()) {
				toReturn = rs.getInt(1);
				System.out.println("AICI " + toReturn);
			}
			System.out.println("SELECT operation on casierBilet completed successfully!");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"AdminDAO:raport2 " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public int raport3() {
int toReturn = 0;
		
		Connection dbConnection = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			dbConnection = ConnectionFactory.getConnection();
			findStatement = dbConnection.prepareStatement(raport3StatementString);
			rs = findStatement.executeQuery();
			if (rs.next()) {
				toReturn = rs.getInt(1);
			}
			System.out.println("SELECT operation on casierBilet completed successfully!");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"AdminDAO:raport3 " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

}