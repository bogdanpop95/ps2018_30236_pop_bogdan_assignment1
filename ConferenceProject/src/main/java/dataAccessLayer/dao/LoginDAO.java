package dataAccessLayer.dao;

import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import dataAccessLayer.connection.ConnectionFactory;
import model.User;

public class LoginDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(LoginDAO.class.getName());
	
	private final static String findStatementString = "SELECT *" + " FROM user" + " WHERE name = ? and password = ?";
	
	public User findUser(String name, String password) {
		User toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setString(1, name);
			findStatement.setString(2, password);
			rs = findStatement.executeQuery();
			if (rs.next()) {
				int idUser = rs.getInt("idUser");
				String role = rs.getString("role");
				int active = rs.getInt("active");
				toReturn = new User(idUser, name, password, role, active);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"LoginDAO:findUser " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
			if (toReturn != null)
				System.out.println("SELECT operation on User with idUser =  " + toReturn.getIdUser() + " completed successfully!");
		}
		return toReturn;
	}
}
