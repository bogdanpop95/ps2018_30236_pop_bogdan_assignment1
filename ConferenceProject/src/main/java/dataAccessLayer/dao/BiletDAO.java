package dataAccessLayer.dao;

import java.util.List;

import model.Bilet;

/**
 * @author pop_b
 * Clasa care inglobeaza operatiile CRUD pe Bilete si mosteneste clasa AbstracDAO
 */
public class BiletDAO extends AbstractDAO<Bilet>{
	
	public BiletDAO() {
		super();
	}
	
	public List<Bilet> findAll() {
		return super.findAll();
	}
	
	public Bilet findById(int idBilet) {
		return super.findById(idBilet);
	}
	
	public int insert(Bilet bilet) {
		return super.insert(bilet);
	}
	
	public void edit(Bilet bilet, String columnName, Object newValue) {
		super.edit(bilet, columnName, newValue);
	}
	
	public void delete(Bilet bilet) throws Exception {
		super.delete(bilet);
	}
}
