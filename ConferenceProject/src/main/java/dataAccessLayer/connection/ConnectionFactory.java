package dataAccessLayer.connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author pop_b
 * Clasa pentru stabilirea conexiunii cu baza de date
 * Singleton, un singur obiect de tipul ConnectionFactory (o singura conexiune la baza de date)
 */
public class ConnectionFactory {
	
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/conferintadb?useSSL=false";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	private static ConnectionFactory singleInstance = new ConnectionFactory();
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metoda care creaza conexiunea cu baza de date pe baza url-ului, user-ului si parolei
	 * @return conexiunea
	 */
	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();
		}
		return connection;
	}
	
	public static Connection getConnection() {
		return singleInstance.createConnection();
	}
	
	/**
	 * Intreruperea conexiunii
	 * @param connection
	 */
	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
	}
	
	/**
	 * Inchiderea unei interogari (operatie pe baza de date)
	 * @param statement interogarea propriu-zisa
	 */
	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
			}
		}
	}
	
	/**
	 * Inchiderea setului de date rezultatat dupa interogare
	 * @param resultSet o lista de date, in urma interogarii, o linie din baza de date
	 */
	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}
}

