package model;

public class Bilet {
	
	private int idBilet;
	private int price;
	private String description;
	private int day; 	// 1-3 days, day 4 means ticket for all 3 days.
	private int disponibile;
	
	public Bilet() {
		
	}
	
	public Bilet(int idBilet, int price, String description, int day, int disponibile) {
		super();
		this.idBilet = idBilet;
		this.price = price;
		this.description = description;
		this.day = day;
		this.disponibile = disponibile;
	}
	
	public Bilet(String idBilet, String price, String description, String day, String disponibile) {
		super();
		this.idBilet = Integer.parseInt(idBilet);
		this.price = Integer.parseInt(price);
		this.description = description;
		this.day = Integer.parseInt(day);
		this.disponibile = Integer.parseInt(disponibile);
	}
	
	public Bilet(int price, String description, int day, int disponibile) {
		super();
		this.price = price;
		this.description = description;
		this.day = day;
		this.disponibile = disponibile;
	}

	
	public int getIdBilet() {
		return idBilet;
	}

	
	public void setIdBilet(int idBilet) {
		this.idBilet = idBilet;
	}

	
	public int getPrice() {
		return price;
	}

	
	public void setPrice(int price) {
		this.price = price;
	}

	
	public String getDescription() {
		return description;
	}

	
	public void setDescription(String description) {
		this.description = description;
	}

	
	public int getDay() {
		return day;
	}

	
	public void setDay(int day) {
		this.day = day;
	}

	public int getDisponibile() {
		return disponibile;
	}

	public void setDisponibile(int disponibile) {
		this.disponibile = disponibile;
	}
	
}
