package model;

public class User {
	
	private int idUser; 	// unique ID
	private String name;
	private String password;
	private String role;	// Admin or Cashier
	private int active;		// User can be active or not (Admin is always 1)
	
	public User() {
		
	}
	
	public User(int idUser, String name, String password, String role, int active) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.password = password;
		this.role = role;
		this.active = active;
	}
	
	public User(String idUser, String name, String password, String role, String active) {
		super();
		this.idUser = Integer.parseInt(idUser);
		this.name = name;
		this.password = password;
		this.role = role;
		this.active = Integer.parseInt(active);
	}
	
	public User(String name, String password, String role, int active) {
		super();
		this.name = name;
		this.password = password;
		this.role = role;
		this.active = active;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdUser() {
		return idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
}
