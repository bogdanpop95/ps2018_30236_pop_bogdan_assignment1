package model;

import java.sql.Time;
import java.util.*;

public class CasierBilet {
	
	private int idBilet;
	private int price;
	private int day;
	private Time soldAt;
	private Date date;
	
	public CasierBilet() {
		
	}
	
	public CasierBilet(int idBilet, int price, int day, Time soldAt, Date date) {
		this.idBilet = idBilet;
		this.price = price;
		this.day = day;
		this.soldAt = soldAt;
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Time getSoldAt() {
		return soldAt;
	}
	
	public void setSoldAt(Time soldAt) {
		this.soldAt = soldAt;
	}

	public int getIdBilet() {
		return idBilet;
	}

	public void setIdBilet(int idBilet) {
		this.idBilet = idBilet;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
}
