-- Comment ALTER TABLE lines for the first run;
ALTER TABLE casierBilet DROP FOREIGN KEY `idBilet_fk`;
ALTER TABLE casierBilet DROP FOREIGN KEY `soldBy_fk`;
DROP TABLE IF EXISTS bilet;
DROP TABLE IF EXISTS casierBilet;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS conference;


CREATE TABLE `conferintadb`.`user` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `role` VARCHAR(20) NOT NULL,
  `active` INT NOT NULL,
  PRIMARY KEY (`idUser`));

CREATE TABLE `conferintadb`.`bilet` (
  `idBilet` INT NOT NULL AUTO_INCREMENT,
  `price` INT NOT NULL,
  `description` VARCHAR(100) DEFAULT NULL,
  `day` INT NOT NULL,
  `disponibile` INT NOT NULL,
  PRIMARY KEY (`idBilet`));

CREATE TABLE `conferintadb`.`casierBilet` (
  `idBilet` INT NOT NULL,
  `soldBy` INT NOT NULL,
  `soldAt` time NOT NULL,
  `date` date NOT NULL,
  KEY `idBilet_idx` (`idBilet`),
  KEY `soldBy_idx` (`soldBy`),
  CONSTRAINT `idBilet_fk` FOREIGN KEY (`idBilet`) REFERENCES `bilet` (`idBilet`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `soldBy_fk` FOREIGN KEY (`soldBy`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE CASCADE);

-- populate user table
INSERT INTO user(idUser, name, password, role, active) VALUES (1, "AdminOne", "admin", "admin", 1);
INSERT INTO user(idUser, name, password, role, active) VALUES (2, "CasierOne", "casier1", "casier", 1);
INSERT INTO user(idUser, name, password, role, active) VALUES (3, "CasierTwo", "casier2", "casier", 1);

-- populate bilet table
INSERT INTO bilet(idBilet, price, description, day, disponibile) VALUES (1, 200, "Amazing description", 1, 100);
INSERT INTO bilet(idBilet, price, description, day, disponibile) VALUES (2, 250, "Amazing description", 2, 75);
INSERT INTO bilet(idBilet, price, description, day, disponibile) VALUES (3, 250, "Amazing description", 3, 75);
INSERT INTO bilet(idBilet, price, description, day, disponibile) VALUES (4, 500, "Amazing description", 4, 25);


INSERT INTO casierBilet(idBilet, soldBy, soldAt, date) VALUES (1, 2, '12:03:02', '2018-03-26');



